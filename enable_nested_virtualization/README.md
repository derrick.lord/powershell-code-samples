# Powershell - Enable Nested Virtualization(Hyper-V only)

##Requirements for Nested Virtualization
In order for nested virtualization to work, you must meet all of the following:

1. The Hyper-V host must be at least the Anniversary Edition version of Windows 10, Windows Server 2016, Hyper-V Server 2016, or Windows Server Semi-Annual Channel
2. The Hyper-V host must be using Intel CPUs. AMD is not yet supported
3. A virtual machine must be off to have its processor extensions changed
