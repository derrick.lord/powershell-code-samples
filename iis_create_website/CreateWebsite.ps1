#--------------------------------------------------------
# Create IIS Website
#--------------------------------------------------------
#



#Parameters (default values)
param (
    [string]$sitename = "newsite", 
    [string]$displayname = "New Site",
    [string]$protocol = "TCP",
	[int32]$localport = 6789,
	[string]$sitepath = ("C:\" + $sitename )
)


New-Website -Name $sitename -PhysicalPath $sitepath -port $localport
New-NetFirewallRule -DisplayName $displayname -Protocol $protocol -LocalPort $localport