"""
	Install AD Rights Management Service

"""

# 1. Install AD CS services
Add-WindowsFeature -Name ADCS-Cert-Authority -IncludeManagementTools

# 2. Perform Default config of Certificate Services as Root CA
Install-AdcsCertificationAuthority -CAType EnterpriseRootCA

# 3. Open Certificate Authority Console
#	a. Open properties of Certificate Authority
#	b. Click 'View Certificate' on General tab (Root Level Certificate)
#	c. Select Details Tab, and click 'Copy to file...'
#	d. Export public key (.cer) for RootCA 
#	e. User GPO to Distribute public key to 'Trusted Root Certification Authorites' on Domain Computers
#
# 4. Install AD RMS Prerequisites
# IIS
Add-WindowsFeature Web-Server -IncludeManagementTools

# ASP.NET
Add-WindowsFeature Web-ASP-NET45 -IncludeManagementTools -IncludeAllSubFeature

# 5. Install AD RMS

Add-WindowsFeature -Name ADRMS -IncludeManagementToolsgp

