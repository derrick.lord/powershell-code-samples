# 1. Install AD CS Role
Add-WindowsFeature ADCS-Cert-Authority -IncludeManagementTools

# 2. Configure CA 
Install-AdcsCertificationAuthority 
	-CAType EnterpriseRootCA 
	-CryptoProviderName "RSA#Microsoft Software Key Storage Provider" 
	-KeyLength 2048 
	-HashAlgorithmName SHA1 
	-ValidityPeriod Years 
	-ValidityPeriodUnits 5