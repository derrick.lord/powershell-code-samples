Add-WindowsFeature -Name AD-Domain-Services –IncludeManagementTools

Install-ADDSForest –DomainName PRACTICEIT.CO.UK –InstallDNS

Add-WindowsFeature –Name ADCS-Cert-Authority –IncludeManagementTools

Install-AdcsCertificationAuthority –CAType EnterpriseRootCA