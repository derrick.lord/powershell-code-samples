Add-WindowsFeature -Name DHCP -IncludeManagementTools -IncludeAllSubFeature


Add-DHCPServerv4Scope -Name "PLABS Scope1" `
 					  -StartRange 192.168.0.200 `
					  -EndRange 192.168.0.220 `
					  -SubnetMask 255.255.255.0 `
					  -Description "Practice Labs DHCP Scope"

Install-WindowsFeature -Name IPAM -IncludeManagementTools -IncludeAllSubFeature

Invoke-IpamGpoProvisioning -Domain practicelabs.com -GpoPrefixName PLABS -IpamServerFqdn plabdm01.practicelabs.com -DelegatedGpoUser administrator