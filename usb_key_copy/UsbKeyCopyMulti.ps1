﻿param($Source)

$drives = Get-WmiObject -Class Win32_logicaldisk -Filter "DriveType = '2'"

foreach($drive in $drives) {
    Write-Output ("Writing file to "+$drive.deviceid+"\")

    Copy-Item "C:\SR_Brochure_2019.06.pdf" -Destination ($drive.deviceid+"\")
    $Eject =  New-Object -comObject Shell.Application
    Write-Output ("Ejecting "+$drive.deviceid)
    $Eject.NameSpace(17).ParseName($drive.deviceid).InvokeVerb(“Eject”)

}