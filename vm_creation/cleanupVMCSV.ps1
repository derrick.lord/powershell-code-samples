﻿#--------------------------------------------------------
# Cleanup (Generation 2) Hyper-V Virtual Machine from CSV
#--------------------------------------------------------



param (
    [string]$fileName = "vminfo.csv"
)



# Constants
#--------------------------------------------------------

$VM_VHD_path = "C:\Users\Public\Documents\Hyper-V\Virtual Hard Disks\"

# Variables
#--------------------------------------------------------
$vms = $()
$vms = Import-Csv (".\"+$filename)


#Iterate through Virtual Machine Collection
#--------------------------------------------------------

foreach($vm in $vms){
    If ((Test-Path ($VM_VHD_path + $vm.vmname + ".vhdx"))) {

        # Remove Virtual Machine
        #--------------------------------------------------------


        Remove-VM -Name $vm.vmname 
        Remove-Item -Path ($VM_VHD_path + $vm.vmname + ".vhdx") -Force -Confirm:$false -ErrorAction SilentlyContinue


    }else{
        Write-Output ("`n"+$VM_VHD_path + $vm.vmname + ".vhdx file not present!!!")
    }
}