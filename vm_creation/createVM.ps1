﻿#--------------------------------------------------------
# Create (Generation 2) Hyper-V Virtual Machine 
#--------------------------------------------------------
#
# Dependencies:
#    1. Create Switch named "InternalNet" or Modify $vmswitch parameter
#    2. Change default VM name by modifying $vmname parameter
#    3. Modify $VM_VHD_path constant to location to store new VM


#Parameters (default values)
param (
    [string]$vmname = "web1", 
    [string]$vmmemory = 1GB,
    [string]$vmswitch = "InternalNet",
    [string]$vmsize = 25GB
)


# Constants
$VM_VHD_path = "C:\Users\Public\Documents\Hyper-V\Virtual Hard Disks\"


# Test .VHDX File presence
#--------------------------------------------------------


If (!(Test-Path ($VM_VHD_path + $vmname + ".vhdx"))) {

    # Create Virtual Machine
    #--------------------------------------------------------
    echo "Creating VM: $vmname"

    New-VM `
        -Name $vmname `
        -NewVHDPath ($VM_VHD_path + $vmname + ".vhdx")`
        -NewVHDSizeBytes $vmsize `
        -MemoryStartupBytes $vmmemory `
        -SwitchName $vmswitch `
        -Generation 2 `

    # Attach Windows 2012 Installation Media (ISO)
    #--------------------------------------------------------

    Add-VMDvdDrive -VMName $vmname -Path "C:\images\Windows Server\9600.17050.WINBLUE_REFRESH.140317-1640_X64FRE_SERVER_EVAL_EN-US-IR3_SSS_X64FREE_EN-US_DV9.iso"

    # Store reference variable to new Virtual Machine
    #--------------------------------------------------------
    $newVM = Get-VM -Name $vmname

} Else {

    Write-Output ("`n"+$VM_VHD_path + $vmname + ".vhdx  already exists!!!")
    Write-Output "`nPlease specify alternate VM name"
    Write-Output "`n`t`tExample: `n`t`t./createVM.ps1 -vmname newVMname"

}




