﻿#--------------------------------------------------------
# Create (Generation 2) Hyper-V Virtual Machine from CSV
#--------------------------------------------------------



param (
    [string]$fileName = "vminfo.csv"
)



# Constants
#--------------------------------------------------------

$VM_VHD_path = "C:\Users\Public\Documents\Hyper-V\Virtual Hard Disks\"

# Variables
#--------------------------------------------------------
$vms = $()
$vms = Import-Csv (".\"+$filename)


#Iterate through Virtual Machine Collection
#--------------------------------------------------------

foreach($vm in $vms){
    If (!(Test-Path ($VM_VHD_path + $vmname + ".vhdx"))) {

        # Create Virtual Machine
        #--------------------------------------------------------


        New-VM `
            -Name $vm.vmname `
            -NewVHDPath ($VM_VHD_path + $vm.vmname + ".vhdx")`
            -NewVHDSizeBytes 25GB `
            -MemoryStartupBytes $vm.vmmemory `
            -SwitchName $vm.vmswitch `
            -Generation 2 


        # Attach Windows 2012 Installation Media (ISO)
        #--------------------------------------------------------

        Add-VMDvdDrive -VMName $vm.vmname -Path "C:\images\Windows Server\9600.17050.WINBLUE_REFRESH.140317-1640_X64FRE_SERVER_EVAL_EN-US-IR3_SSS_X64FREE_EN-US_DV9.iso"


    }else{
        Write-Output ("`n"+$VM_VHD_path + $vm.vmname + ".vhdx  already exists!!!")
    }
}